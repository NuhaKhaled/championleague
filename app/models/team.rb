class Team < ActiveRecord::Base
	 has_and_belongs_to_many :champion_matches
	 has_many :league_values
end
