class CreateLeagueValues < ActiveRecord::Migration
  def change
    create_table :league_values do |t|
      t.integer :pts
      t.integer :played
      t.integer :won
      t.integer :draw
      t.integer :lost
      t.integer :goal_difference
      t.integer :team_id
      t.integer :top_rate

      t.timestamps
    end
  end
end
