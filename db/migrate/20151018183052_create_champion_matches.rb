class CreateChampionMatches < ActiveRecord::Migration
  def change
    create_table :champion_matches do |t|
      t.integer :week_counter
      t.integer :no_of_weeks

      t.timestamps
    end
  end
end
