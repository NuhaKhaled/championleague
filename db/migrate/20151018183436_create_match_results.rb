class CreateMatchResults < ActiveRecord::Migration
  def change
    create_table :match_results do |t|
      t.integer :week_no
      t.integer :team_one_id
      t.integer :team_two_id
      t.integer :score_one
      t.integer :score_two
      t.integer :champion_match_id

      t.timestamps
    end
  end
end
